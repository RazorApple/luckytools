alias luck_restart="ssh yourOwnServer 'systemctl --user restart nodejs'"
alias luck_status="ssh yourOwnServer 'systemctl --user status -n60 nodejs'"

# luck_push removed because I use SmartGit for all of my
# pushing now.

luck_update() {
        ssh yourOwnServer "sed -i 's/$@/' luckbot/config.json && cat luckbot/config.json" && luck_restart
}
